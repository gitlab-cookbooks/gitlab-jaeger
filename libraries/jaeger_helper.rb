module Gitlab
  module Jaeger
    def self.agent_unit(node)
      binary = node['jaeger-agent']['binary']
      flags = kingpin_flags_for(node, 'jaeger-agent')
      jaeger_cmd = [binary, flags].join(' ')
      jaeger_env = node['jaeger-agent']['env'].map do |var, value|
        "\"#{var}=#{value}\""
      end.join(' ')

      jaeger_unit = {
        Unit: {
          Description: 'Jaeger Agent',
          Documentation: ['https://www.jaegertracing.io/'],
          After: 'network.target',
        },
        Service: {
          Type: 'simple',
          Environment: jaeger_env,
          ExecReload: '/bin/kill -HUP $MAINPID',
          ExecStart: jaeger_cmd,
          KillMode: 'process',
          MemoryLimit: "#{node['jaeger-agent']['memory_kb']}K",
          LimitNOFILE: '1000000',
          Restart: 'always',
          RestartSec: '5s',
          User: node['jaeger']['user'],
        },
        Install: {
          WantedBy: 'multi-user.target',
        },
      }

      jaeger_unit
    end

    def self.kingpin_flags_for(node, service)
      config = ''
      node[service]['flags'].each do |flag_key, flag_value|
        if flag_value == true
          config += "--#{flag_key} "
        elsif flag_value == false
          config += "--no-#{flag_key} "
        elsif flag_value.is_a?(Array)
          flag_value.each do |multi_flag_value|
            config += "--#{flag_key}=#{multi_flag_value} " unless multi_flag_value.empty?
          end
        else
          config += "--#{flag_key}=#{flag_value} " unless flag_value.empty?
        end
      end
      config
    end
  end
end

Chef::Recipe.send(:include, Gitlab::Jaeger)
Chef::Resource.send(:include, Gitlab::Jaeger)
Chef::Provider.send(:include, Gitlab::Jaeger)
