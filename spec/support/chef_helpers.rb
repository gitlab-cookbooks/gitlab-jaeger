module ChefHelpers
  def stub_node(attrs)
    address =
      case attrs['hostname']
      when 'my.hostname'
        '192.168.1.2'
      when 'labeled.hostname'
        '192.168.1.3'
      end

    interfaces =
      if address
        { 'eth0' => { 'addresses' => { address => { 'family' => 'inet' } } } }
      else
        {}
      end

    attrs.merge('network' => { 'interfaces' => interfaces })
  end
end
