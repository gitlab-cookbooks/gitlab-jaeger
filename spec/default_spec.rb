require 'spec_helper'

describe 'gitlab-jaeger::default' do
  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'creates a jaeger user and group' do
      expect(chef_run).to create_user('gitlab-jaeger').with(
        system: true,
        shell: '/bin/false'
      )
    end

    it 'creates the bin directory' do
      expect(chef_run).to create_directory('/opt/jaeger/bin').with(
        owner: 'gitlab-jaeger',
        group: 'gitlab-jaeger',
        mode: '0755',
        recursive: true
      )
    end

    it 'installs jaeger' do
      expect(chef_run).to put_ark('bin')
    end
  end
end
