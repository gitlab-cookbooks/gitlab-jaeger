require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

describe 'gitlab-jaeger::agent' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.override['jaeger-agent']['collector'] = 'jaeger-collector.jaeger-infra.svc:14250'
        node.override['jaeger-agent']['tags'] = 'foo=bar'
      end.converge('consul::default', described_recipe)
    end

    let(:node) { chef_run.node }

    it 'creates the log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/jaeger/jaeger-agent').with(
        owner: 'gitlab-jaeger',
        group: 'gitlab-jaeger',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates systemd units' do
      expect(chef_run).to create_systemd_unit('jaeger-agent.service').with(
        content: {
          Unit: {
            Description: 'Jaeger Agent',
            Documentation: ['https://www.jaegertracing.io/'],
            After: 'network.target',
          },
          Service: {
            Type: 'simple',
            Environment: '',
            ExecReload: '/bin/kill -HUP $MAINPID',
            ExecStart: '/opt/jaeger/bin/jaeger-agent --reporter.grpc.host-port=jaeger-collector.jaeger-infra.svc:14250 --jaeger.tags=foo=bar ',
            KillMode: 'process',
            MemoryLimit: '1048576K',
            LimitNOFILE: '1000000',
            Restart: 'always',
            RestartSec: '5s',
            User: 'gitlab-jaeger',
          },
          Install: {
            WantedBy: 'multi-user.target',
          },
        }
      )
    end
  end
end
