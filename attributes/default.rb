#
# Cookbook:: GitLab::Jaeger
# Attributes:: default
#

default['jaeger']['user']       = 'gitlab-jaeger'
default['jaeger']['group']      = 'gitlab-jaeger'
default['jaeger']['dir']        = '/opt/jaeger'
default['jaeger']['bin_dir']    = '/opt/jaeger/bin'

default['jaeger']['version']    = '1.19.2'
default['jaeger']['checksum']   = '56e4be185f25fddb9406bb1982c5692c308dc687a7799d9d725deb084d8e874a'
default['jaeger']['binary_url'] = "https://github.com/jaegertracing/jaeger/releases/download/v#{node['jaeger']['version']}/jaeger-#{node['jaeger']['version']}-linux-amd64.tar.gz"
