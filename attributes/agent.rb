#
# Cookbook:: gitlab-jaeger
# Attributes:: agent
#
# binds to ports:
# - thrift: 6831
# - config: 5778
# - admin: 14271

default['jaeger-agent']['enable']      = true
default['jaeger-agent']['collector']   = nil
# default['jaeger-agent']['collector']   = 'jaeger-collector.jaeger-infra.svc:14250'
default['jaeger-agent']['tags']        = nil

default['jaeger-agent']['binary']      = "#{node['jaeger']['bin_dir']}/jaeger-agent"
default['jaeger-agent']['log_dir']     = '/var/log/jaeger/jaeger-agent'

default['jaeger-agent']['memory_kb'] = 1048576 # 1GiB
default['jaeger-agent']['env']       = {}
default['jaeger-agent']['flags']     = {}
