gitlab-jaeger Cookbook
==========================

This cookbook installs and sets up the jaeger agent.

### Configuration

```
{
  "jaeger-agent": {
    "collector": "jaeger-collector.jaeger-infra.svc:14250",
    "tags": "foo=bar"
  }
}
```
