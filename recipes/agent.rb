require 'yaml'
include_recipe 'gitlab-jaeger::default'

directory node['jaeger-agent']['log_dir'] do
  owner node['jaeger']['user']
  group node['jaeger']['group']
  mode '0755'
  recursive true
end

if node['jaeger-agent']['collector']
  node.default['jaeger-agent']['flags']['reporter.grpc.host-port'] = node['jaeger-agent']['collector']
end

if node['jaeger-agent']['tags']
  node.default['jaeger-agent']['flags']['jaeger.tags'] = node['jaeger-agent']['tags']
end

systemd_unit 'jaeger-agent.service' do
  content Gitlab::Jaeger.agent_unit(node)
  action [:create, :enable, :start]
  subscribes :restart, 'ark[jaeger-agent]', :delayed
  only_if { node['jaeger-agent']['enable'] }
  notifies :restart, 'systemd_unit[jaeger-agent.service]', :delayed
end

# Consul registration.

if !node['gitlab_consul'].nil? && node['gitlab_consul']['agent']['enabled'] == true
  admin_port = 14271
  consul_definition 'jaeger-agent-admin' do
    type 'service'
    parameters(
      check: {
        http: "http://127.0.0.1:#{admin_port}",
        interval: '15s',
      },
      name: 'jaeger-agent-admin',
      port: admin_port.to_i,
      tags: %w(metrics)
    )
    only_if { node['jaeger-agent']['enable'] }
    notifies :reload, 'consul_service[consul]', :delayed
  end
end
