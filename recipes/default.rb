#
# Cookbook:: gitlab-monitoring
# Recipe:: default
#
# Copyright:: 2016, GitLab
#
# All rights reserved - Do Not Redistribute
#

user node['jaeger']['user'] do
  system true
  shell '/bin/false'
  home node['jaeger']['dir']
  not_if node['jaeger']['user'] == 'root'
end

directory node['jaeger']['dir'] do
  owner node['jaeger']['user']
  group node['jaeger']['group']
  mode '0755'
  recursive true
end

directory node['jaeger']['bin_dir'] do
  owner node['jaeger']['user']
  group node['jaeger']['group']
  mode '0755'
  recursive true
end

ark ::File.basename(node['jaeger']['bin_dir']) do
  url node['jaeger']['binary_url']
  checksum node['jaeger']['checksum']
  version node['jaeger']['version']
  prefix_root Chef::Config['file_cache_path']
  path ::File.dirname(node['jaeger']['bin_dir'])
  owner node['jaeger']['user']
  group node['jaeger']['group']
  action :put
end
